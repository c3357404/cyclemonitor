﻿using CycleMonitor.Site.Database;
using CycleMonitor.Site.Dto;
using CycleMonitor.Site.Helpers;
using CycleMonitor.Site.Models;
using Dropbox.Api;
using Dropbox.Api.Files;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CycleMonitor.Site.Controllers
{
    public class HomeController : Controller
    {
        private CycleMonitorContext _entities = new CycleMonitorContext();
        private static readonly string _dbxAccessToken = "aE6WwPDUGtAAAAAAAAAAB0kUb33iTHKTwc2z4FYRA5frs_9jWGuxrpc0Sje8pf-u";

        public HomeController()
        {
        }

        public ActionResult Index()
        {
            var dates = _entities.CycleSessions
                .Where(c => c.FileName != null)
                .Select(s => s.DateTime)
                .ToList();

            if (dates.Count > 0)
            {
                var model = new IndexModel()
                {
                    MinDate = dates.OrderBy(s => s).FirstOrDefault(),
                    MaxDate = dates.OrderByDescending(s => s).FirstOrDefault()
                };

                return View("Index", model);
            }
            else
            {
                return RedirectToAction("FileUpload");
            }
        }

        public ActionResult GetSchedulerData([DataSourceRequest]DataSourceRequest request)
        {
            var schedule = _entities.CycleSessions
                .Where(c => c.FileName != null)
                .GroupBy(c => c.FileName)
                .Select(s => s.FirstOrDefault())
                .ToList();

            var result = schedule
                .Select(s => s.ToSchedulerModel())
                .ToList();

            var response = new DataSourceResult()
            {
                Data = result != null ? result : null,
                Total = result != null ? result.Count : 0
            };

            return Json(response);
        }

        public ActionResult FileUpload()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UploadFile(FileUploadModel model)
        {
            if (ModelState.IsValid)
            {
                var fileName = Path.GetFileName(model.File.FileName);
                if (Path.GetExtension(model.File.FileName) == ".hrm")
                {
                    if (model.File != null && model.File.ContentLength > 0)
                    {
                        var dto = new CycleSessionDto();
                        using (var file = model.File.InputStream)
                        {
                            // upload file we have to dropbox
                            try
                            {
                                await UploadToDropbox(fileName, file);
                            }
                            catch (Exception ex)
                            {
                                ModelState.AddModelError("", ex);

                                return View("FileUpload", model);
                            }

                            file.Position = 0;
                            // Parse the file
                            dto = FileParseHelper.ParseFileFromPath(file, true);
                            dto.FileName = fileName;
                        }

                        // Save the data we have parsed into a dto to the db
                        var entity = dto.ToEntity();
                        _entities.CycleSessions.Add(entity);
                        _entities.SaveChanges();

                        // Make a model from the dto
                        CycleSessionModel cycleSessionModel = dto.ToModel();
                        cycleSessionModel.FileName = dto.FileName;
                        cycleSessionModel.IsImperial = dto.IsImperial;

                        return View("Data", cycleSessionModel);
                    }

                    throw new Exception("Could not upload file as it was empty!");
                }
                else
                {
                    ModelState.AddModelError("File", "File type must be .hrm");
                }
            }

            return View("FileUpload", model);
        }        
        
        public async Task<ActionResult> Data(Guid id)
        {
            var session = _entities.CycleSessions
                .Where(c => c.SessionId == id)
                .SingleOrDefault();

            Stream dbxFile;

            using (var dbx = new DropboxClient(_dbxAccessToken))
            {
                try
                {
                    dbxFile = await DownloadFromDropbox(dbx, session.FileName);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                if (dbxFile != null)
                {
                    var dto = new CycleSessionDto();

                    using (var file = new MemoryStream())
                    {
                        dbxFile.CopyTo(file);
                        file.Position = 0;
                        dto = FileParseHelper.ParseFileFromPath(file, true);
                    }

                    CycleSessionModel model = dto.ToModel();
                    model.SessionId = session.SessionId;
                    model.FileName = session.FileName;
                    model.Interval = session.Interval;
                    model.IsImperial = dto.IsImperial;

                    return View("Data", model); throw new Exception("Could not find a file for this record");
                }
                else
                {
                    throw new Exception("Could not find a file related to this record!");
                }
            }      
        }

        [HttpPost]
        public async Task<ActionResult> GenerateGraphView(string fileName, DateTime startDate, int interval, int ftp, bool isImperial, DateTime? timeFrom, DateTime? timeTo)
        {
            Stream dbxFile;

            using (var dbx = new DropboxClient(_dbxAccessToken))
            {
                try
                {
                    dbxFile = await DownloadFromDropbox(dbx, fileName);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                if (dbxFile != null)
                {
                    var dtos = new List<HeartRateDto>();

                    using (var file = new MemoryStream())
                    {
                        dbxFile.CopyTo(file);
                        file.Position = 0;
                        dtos = FileParseHelper.ParseHeartRateInfoFromPath(file, startDate, interval, isImperial);
                    }

                    if (timeFrom.HasValue)
                    {
                        dtos = dtos.Where(w => w.Time.TimeOfDay >= timeFrom.Value.TimeOfDay).ToList();
                    }

                    if (timeTo.HasValue)
                    {
                        dtos = dtos.Where(w => w.Time.TimeOfDay <= timeTo.Value.TimeOfDay).ToList();
                    }

                    var intervals = IntervalDetectionHelper.DetectInterval(dtos, isImperial);

                    var model = new CycleSessionDataModel()
                    {
                        FileName = fileName,
                        HeartRateDetails = dtos,
                        Interval = interval,
                        UseImperial = isImperial
                    };

                    var duration = dtos.Select(s => s.Time).Last() - dtos.Select(s => s.Time).First();
                    var normalisedPower = CogganHelper.CalculateNormalisedPower(dtos.Select(s => s.Power).ToList(), interval);
                    var intensityFactor = CogganHelper.CalculateIntensityFactor(normalisedPower, ftp);
                    var tss = CogganHelper.CalculateTrainingStressScore(duration.TotalSeconds, normalisedPower, intensityFactor, ftp);
                    var workoutIntervals = IntervalDetectionHelper.DetectInterval(dtos, isImperial);

                    model.NormalisedPower = normalisedPower;
                    model.IntensityFactor = intensityFactor;
                    model.TrainingStressScore = tss;
                    model.WorkoutIntervals = workoutIntervals;

                    //return Json(dtos);
                    return PartialView("_Graphs", model);
                }
                else
                {
                    throw new Exception("Couldn't read the Heart Rate data for file");
                }
            }
        }

        [Obsolete]
        public ActionResult CycleSessions()
        {
            var model = new CycleSessionModel();

            return View(model);
        }

        private async Task UploadToDropbox(string fileName, Stream file)
        {
            StreamReader stream = new StreamReader(file);
            using (var mem = new MemoryStream(Encoding.UTF8.GetBytes(stream.ReadToEnd())))
            {
                using (var dbx = new DropboxClient(_dbxAccessToken))
                {
                    await dbx.Files.UploadAsync("/Files/Upload/" + fileName, WriteMode.Overwrite.Instance, body: mem);
                }                
            }
        }

        private async Task<Stream> DownloadFromDropbox(DropboxClient dbx, string fileName)
        {
            var path = Path.Combine("/Files/Upload/", fileName);
            var response = await dbx.Files.DownloadAsync(path);
            var result = await response.GetContentAsStreamAsync();

            return result;
        }

        
    }
}