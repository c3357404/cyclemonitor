namespace CycleMonitor.Site.Database
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CycleSession",
                c => new
                    {
                        SessionId = c.Guid(nullable: false, identity: true),
                        DateTime = c.DateTime(nullable: false),
                        SessionLengthTicks = c.Long(nullable: false),
                        Interval = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SessionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CycleSession");
        }
    }
}
