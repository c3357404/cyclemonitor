namespace CycleMonitor.Site.Database
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CycleSession_AddFileNameProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CycleSession", "FileName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CycleSession", "FileName");
        }
    }
}
