﻿using CycleMonitor.Site.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CycleMonitor.Site.Helpers
{
    public static class CogganHelper
    {
        public static double CalculateNormalisedPower(List<int> power, int interval)
        {
            // Do this step by step so it's actually readable code
            // NOTE: Bear in mind that this will only calculate the Normalised Power for the first 30 seconds of the data passed to the method!

            // 1.Starting at the beginning of the data and calculating a 30 - second rolling average for power;
            // 2.Raising the values obtained in step 1 to the fourth power;
            // 3.Taking the average of all the values obtained in step 2; and
            // 4.Taking the fourth root of the number obtained in step 3.This is Normalized Power.
            // Hunter, Allen(2010 - 04 - 01).Training and Racing with a Power Meter, 2nd Ed. (Kindle Locations 2723 - 2730). VeloPress.Kindle Edition.
            var powerCount = power.Count;
            var toTake = 30 / interval;

            List<double> rollingPowers = new List<double>();

            for (var i = 0; i <= (powerCount - toTake); i++)
            {
                var pd = power.Skip(i).Take(toTake).Average();

                rollingPowers.Add(pd);
            }

            if (rollingPowers.Count >= 1)
            {
                List<double> powerFourth = new List<double>();

                foreach (var rollingPower in rollingPowers)
                {
                    powerFourth.Add(Math.Pow(rollingPower, 4));
                }


                var powerFourthAverage = powerFourth.Average();
                var normalisedPower = Math.Pow(powerFourthAverage, 1.0 / 4);

                normalisedPower = Math.Round(normalisedPower, 2);

                return normalisedPower;
            }

            return 0;
        }

        public static double CalculateIntensityFactor(double normalisedPower, int functionalThresholdPower)
        {
            var iF = normalisedPower / functionalThresholdPower;
            iF = Math.Round(iF, 2);

            return iF;
        }

        public static double CalculateTrainingStressScore(double seconds, double normalisedPower, double intensityFactor, int functionalThresholdPower)
        {
            //TSS = (sec x NP® x IF®)/(FTP x 3600) x 100

            var tss = (seconds * normalisedPower * intensityFactor) / (functionalThresholdPower * 3600) * 100;

            tss = Math.Round(tss, 2);

            return tss;
        }
    }
}