﻿using CycleMonitor.Site.Dto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace CycleMonitor.Site.Helpers
{
    public static class FileParseHelper
    {
        public static CycleSessionDto ParseFileFromPath(Stream file, bool parseHeartRate)
        {
            var streamReader = new StreamReader(file);
            var cycleSessionDto = new CycleSessionDto();
            cycleSessionDto.HeartRateDetails = new List<HeartRateDto>();

            string line;

            while ((line = streamReader.ReadLine()) != null)
            {
                if (line.Contains("SMode="))
                {
                    SetMeasurementType(line, cycleSessionDto);
                }
                if (line.Contains("Interval="))
                {
                    SetInterval(line, cycleSessionDto);
                }
                if (line.Contains("Date="))
                {
                    SetDate(line, cycleSessionDto);
                }

                if (line.Contains("StartTime="))
                {
                    SetStartTime(line, cycleSessionDto);
                }

                if (line.Contains("Length="))
                {
                    SetSessionLengthTicks(line, cycleSessionDto);
                }

                if (parseHeartRate)
                {
                    if (line.Contains("[HRData]"))
                    {
                        ParseHeartRateDataForParent(streamReader, cycleSessionDto, cycleSessionDto.IsImperial);
                    }
                }
            }
            streamReader.Dispose();
            return cycleSessionDto;
        }

        public static List<HeartRateDto> ParseHeartRateInfoFromPath(Stream file, DateTime startTime, int interval, bool useImperial)
        {
            var streamReader = new StreamReader(file);
            var dtos = new List<HeartRateDto>();

            string line;

            while ((line = streamReader.ReadLine()) != null)
            {
                if (line.Contains("[HRData]"))
                {
                    ParseHeartRateData(streamReader, dtos, startTime, interval, useImperial);
                }
            }

            streamReader.Dispose();

            return dtos;
        }

        private static void ParseHeartRateDataForParent(StreamReader streamReader, CycleSessionDto dto, bool isImperial)
        {
            string hrLine = String.Empty;
            while ((hrLine = streamReader.ReadLine()) != null)
            {
                var heartRateDto = new HeartRateDto();
                string[] lineSections = hrLine.Split('\t');
                
                if (dto.HeartRateDetails.Count > 0)
                {
                    heartRateDto.Time = dto.HeartRateDetails.Last().Time.AddSeconds(dto.Interval);
                }
                else
                {
                    heartRateDto.Time = dto.DateTime.AddSeconds(dto.Interval);
                }

                heartRateDto.BeatsPerMinute = Int32.Parse(lineSections[0]);
                heartRateDto.Speed = isImperial ? Int32.Parse(lineSections[1]) * 0.621371 / 10 : Int32.Parse(lineSections[1]) / 10;
                heartRateDto.Cadence = Int32.Parse(lineSections[2]);
                heartRateDto.Altitude = isImperial ? Int32.Parse(lineSections[3]) * 3.28 : Int32.Parse(lineSections[3]);
                heartRateDto.Power = Int32.Parse(lineSections[4]);
                heartRateDto.Distance = (heartRateDto.Speed / (dto.Interval * 3600));

                dto.HeartRateDetails.Add(heartRateDto);
            }
        }

        private static void ParseHeartRateData(StreamReader streamReader, List<HeartRateDto> dtos, DateTime startTime, int interval, bool isImperial)
        {
            string hrLine = String.Empty;
            while ((hrLine = streamReader.ReadLine()) != null)
            {
                var dto = new HeartRateDto();
                string[] lineSections = hrLine.Split('\t');

                if (dtos.Count > 0)
                {
                    dto.Time = dtos.Last().Time.AddSeconds(interval);
                }
                else
                {
                    dto.Time = startTime.AddSeconds(interval);
                }

                dto.BeatsPerMinute = Int32.Parse(lineSections[0]);
                dto.Speed = isImperial ? Int32.Parse(lineSections[1]) * 0.621371 / 10 : Int32.Parse(lineSections[1]) / 10;
                dto.Cadence = Int32.Parse(lineSections[2]);
                dto.Altitude = isImperial ? Int32.Parse(lineSections[3]) * 3.28 : Int32.Parse(lineSections[3]);
                dto.Power = Int32.Parse(lineSections[4]);
                //heartRateDto.PowerBalance = Int32.Parse(lineSections[5]);
                dto.Distance = (dto.Speed / (interval * 3600));

                dtos.Add(dto);
            }
        }

        #region ParseParams
        private static void SetDate(string line, CycleSessionDto dto)
        {
            string[] lineSections = line.Split('=');

            var dateTimeString = lineSections.Last();
            DateTime dateTime = DateTime.ParseExact(dateTimeString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);

            dto.DateTime = dateTime;
        }

        private static void SetInterval(string line, CycleSessionDto dto)
        {
            string[] lineSections = line.Split('=');

            dto.Interval = Int32.Parse(lineSections.Last());
        }

        private static void SetStartTime(string line, CycleSessionDto dto)
        {
            string[] lineSections = line.Split('=');

            var dateTimeString = lineSections.Last();
            TimeSpan time = new TimeSpan();

            TimeSpan.TryParse(dateTimeString, out time);

            dto.DateTime = dto.DateTime.Add(time);
        }

        private static void SetSessionLengthTicks(string line, CycleSessionDto dto)
        {
            string[] lineSections = line.Split('=');

            var dateTimeString = lineSections.Last();
            TimeSpan time = new TimeSpan();

            TimeSpan.TryParse(dateTimeString, out time);

            dto.SessionLengthTicks = time.Ticks;
        }

        public static void SetMeasurementType(string line, CycleSessionDto dto)
        {
            var type = line.ToCharArray().Last();

            dto.IsImperial = type == '1';
        }
    #endregion
    }
}