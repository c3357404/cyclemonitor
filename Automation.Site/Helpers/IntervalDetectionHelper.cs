﻿using CycleMonitor.Site.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CycleMonitor.Site.Helpers
{
    public static class IntervalDetectionHelper
    {
        public static List<WorkoutIntervalDto> DetectInterval(List<HeartRateDto> dtos, bool isImperial)
        {
            List<WorkoutIntervalDto> workoutIntervals = new List<WorkoutIntervalDto>();
            WorkoutIntervalDto period = new WorkoutIntervalDto(isImperial);
            period.HeartRateDetails = new List<HeartRateDto>();
            bool foo;

            foreach (var dto in dtos)
            {

                // Must go first otherwise it will add a new period for every thing that comes into the loop
                if (dto.Power >= 200 && dto.Equals(dtos.Last()))
                {
                    period.HeartRateDetails.Add(dto);
                    workoutIntervals.Add(period);
                }
                else if (dto.Power >= 200)
                {
                    period.HeartRateDetails.Add(dto);
                }
                else if (dto.Power < 120 && period.HeartRateDetails.Count > 0)
                {
                    if ((period.EndTime - period.StartTime).TotalSeconds > 10)
                    {
                        workoutIntervals.Add(period);

                        period = new WorkoutIntervalDto(isImperial);
                        period.HeartRateDetails = new List<HeartRateDto>();
                    }
                }
            }

            return workoutIntervals;
        }
    }
}