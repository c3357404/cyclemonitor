﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CycleMonitor.Site.Dto
{
    public class HeartRateDto
    {

        public Guid SessionId { get; set; }

        public DateTime Time { get; set; }

        public int BeatsPerMinute { get; set; }

        public double Speed { get; set; }

        public int Cadence { get; set; }

        public double Altitude { get; set; }

        public int Power { get; set; }

        public int? PowerBalance { get; set; }

        public double Distance { get; set; }
    }
}
