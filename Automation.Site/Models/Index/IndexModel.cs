﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CycleMonitor.Site.Models
{
    public class IndexModel
    {
        public DateTime MinDate { get; set; }

        public DateTime MaxDate { get; set; }
    }
}