﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CycleMonitor.Site.Models
{
    public class CycleMonitorSchedulerModel : ISchedulerEvent
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public string StartTimezone { get; set; }

        public string EndTimezone { get; set; }

        public bool IsAllDay { get; set; }

        public string RecurrenceException { get; set; }

        public string RecurrenceRule { get; set; }    

        //

        public Guid SessionId { get; set; }

        public DateTime DateTime { get; set; }
    }
}