﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CycleMonitor.Site.Models
{
    public class BootgridResult<T>
    {
        public BootgridResult()
        {
            current = 1;
        }

        public int current { get; set; }

        public int rowCount { get; set; }

        public int total { get; set; }

        public IEnumerable<T> rows { get; set; }
    }
}