﻿using CycleMonitor.Site.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CycleMonitor.Site.Models
{
    public class CycleSessionDataModel
    {
        public List<HeartRateDto> HeartRateDetails { get; set; }

        public List<WorkoutIntervalDto> WorkoutIntervals { get; set; }

        [Display(Name = "Interval")]
        public int Interval { get; set; }

        [Display(Name = "TotalDistance")]
        public double TotalDistance
        {
            get
            {
                return Math.Round(HeartRateDetails.Sum(s => s.Distance), 2);
            }
        }

        [Display(Name = "AverageSpeed")]
        public double AverageSpeed
        {
            get
            {
                return Math.Round(HeartRateDetails.Average(s => s.Speed), 2);
            }
        }

        [Display(Name = "TopSpeed")]
        public double TopSpeed
        {
            get
            {
                return Math.Round(HeartRateDetails.OrderByDescending(h => h.Speed).Select(s => s.Speed).First(), 2);
            }
        }

        [Display(Name = "AverageHeartRate")]
        public double AverageHeartRate
        {
            get
            {
                return Math.Round(HeartRateDetails.Average(s => s.BeatsPerMinute), 2);
            }
        }

        [Display(Name = "MaximumHeartRate")]
        public double MaximumHeartRate
        {
            get
            {
                return HeartRateDetails.OrderByDescending(h => h.BeatsPerMinute).Select(s => s.BeatsPerMinute).First();
            }
        }

        [Display(Name = "MinimumHeartRate")]
        public double MinimumHeartRate
        {
            get
            {
                return HeartRateDetails.OrderBy(h => h.BeatsPerMinute).Select(s => s.BeatsPerMinute).First();
            }
        }

        [Display(Name = "AveragePower")]
        public double AveragePower
        {
            get
            {
                return Math.Round(HeartRateDetails.Average(s => s.Power), 2);
            }
        }

        [Display(Name = "Maximum Power")]
        public int MaximumPower
        {
            get
            {
                return HeartRateDetails.OrderByDescending(h => h.Power).Select(s => s.Power).First();
            }
        }

        [Display(Name = "AverageAltitude")]
        public double? AverageAltitude
        {
            get
            {
                return Math.Round(HeartRateDetails.Average(s => s.Altitude), 2);
            }
        }

        [Display(Name = "MaximumAltitude")]
        public double? MaximumAltitude
        {
            get
            {
                return Math.Round(HeartRateDetails.OrderByDescending(h => h.Altitude).Select(s => s.Altitude).First(), 2);
            }
        }

        public double TotalTime
        {
            get
            {
                var time = HeartRateDetails.OrderByDescending(s => s.Time).Select(s => s.Time);
                var latestTime = time.First();
                var earliestTime = time.Last();

                var totalTime = latestTime - earliestTime;
                return totalTime.TotalMinutes;
            }
        }

        [Display(Name = "AverageNormalisedPower")]
        public double NormalisedPower { get; set; }

        public double IntensityFactor { get; set; }

        public double TrainingStressScore { get; set; }

        [Display(Name = "FileName")]
        public string FileName { get; set; }

        [Display(Name = "UseImperial")]
        public bool UseImperial { get; set; }
    }
}