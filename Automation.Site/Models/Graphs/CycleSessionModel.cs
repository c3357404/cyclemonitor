﻿using CycleMonitor.Site.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CycleMonitor.Site.Models
{
    public class CycleSession
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid SessionId { get; set; }

        public DateTime DateTime { get; set; }

        public Int64 SessionLengthTicks { get; set; }

        public int Interval { get; set; }

        public string FileName { get; set; }

        public CycleMonitorSchedulerModel ToSchedulerModel()
        {
            return new CycleMonitorSchedulerModel()
            {
                Title = string.Format("{0} {1}", "Cycle Session:", this.DateTime.ToString("dd/MM/yy")),
                Description = string.Format("{0} {1}", "Cycle Session for date:", this.DateTime.ToShortDateString()),
                Start = this.DateTime,
                End = this.DateTime,
                SessionId = this.SessionId,
                DateTime = this.DateTime
            };
        }
    }

    public class CycleSessionModel
    {
        public Guid SessionId { get; set; }

        public string FileName { get; set; }

        [Display(Name = "DateTime")]
        public DateTime DateTime { get; set; }

        [Display(Name = "SessionLengthTicks")]
        public Int64 SessionLengthTicks { get; set; }

        [Display(Name = "Interval")]
        public int Interval { get; set; }

        public List<HeartRateModel> HeartRateDetails { get; set; }

        public bool IsImperial { get; set; }
    }
}