﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CycleMonitor.Site.Models
{
    public class HeartRateModel
    {
        [Display(Name = "SessionId")]
        public Guid SessionId { get; set; }

        [Display(Name = "Time")]
        public DateTime Time { get; set; }

        [Display(Name = "BeatsPerMinute")]
        public int BeatsPerMinute { get; set; }

        [Display(Name = "Speed")]
        public double Speed { get; set; }

        [Display(Name = "Cadence")]
        public int Cadence { get; set; }

        [Display(Name = "Altitude")]
        public double? Altitude { get; set; }

        [Display(Name = "Power")]
        public int Power { get; set; }

        [Display(Name = "PowerBalance")]
        public int? PowerBalance { get; set; }

        [Display(Name = "Distance")]
        public double Distance { get; set; }
    }
}