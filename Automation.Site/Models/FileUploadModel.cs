﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace CycleMonitor.Site.Models
{
    public class FileUploadModel
    {

        [Required]
        [Display(Name = "FileName")]
        public HttpPostedFileBase File { get; set; }
    }
}